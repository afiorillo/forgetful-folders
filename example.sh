#!/usr/bin/env bash
# This file serves as both example usage, and as a simple test of the script.
set -eux

./forgetful_folders.py /tmp/.ramdisk1 /tmp/.ramdisk2

# we can write files into those folders
touch /tmp/.ramdisk1/hello
touch /tmp/.ramdisk2/hello

# we can delete one of them, and rerun the script and it's fine
rm -rf /tmp/.ramdisk2
./forgetful_folders.py /tmp/.ramdisk1 /tmp/.ramdisk2

# and then let's clean up
rm -rf /tmp/.ramdisk1 /tmp/.ramdisk2
