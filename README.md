# Forgetful Folders

A flexible temporary filesystem to help keep your computer clean.

Forgetful Folders are directories which keep files in memory rather than on a disk.
This makes read and write very fast, but also means that when you computer powers down the **contents will be lost**.
This can be desirable: if you want to keep some folders clean (e.g. browser downloads), then Forgetful Folders are for you.


[![pipeline status](https://gitlab.com/afiorillo/forgetful-folders/badges/main/pipeline.svg)](https://gitlab.com/afiorillo/forgetful-folders/-/commits/main)

[![Latest Release](https://gitlab.com/afiorillo/forgetful-folders/-/badges/release.svg)](https://gitlab.com/afiorillo/forgetful-folders/-/releases)
## Usage

By default, the script creates a single `~/.ramdisk` directory.

```python
$ ./forgetful_folders.py
$ ls ~/.ramdisk
total 0
```

It accepts a list of directories as subsequent arguments (and then does not create the default). I.e.

```python
$ ./forgetful_folders.py ~/.ramdisk ~/Downloads/unsorted
```

## Installation

This script is runnable with Python standard lib only, and only support Linux operating systems.
You should already have Python 3.6+ installed and can just run the script, if not then [install it](https://www.python.org/downloads/).

You can download the script directly from Gitlab

```bash
$ wget -O ~/.local/bin/forgetful_folders https://gitlab.com/afiorillo/forgetful-folders/-/raw/main/forgetful_folders.py
# assuming ~/.local/bin is on your PATH
$ forgetful_folders
```

### At startup with systemd

You can use `systemd` to execute the script at startup as a given user (allowing `~` to expand to that user's home).
An example systemd unit is given in `install/forgetful_folders.service`.
To install:

```bash
$ mkdir -p ~/.config/systemd/user
$ cp install/forgetful_folders.service ~/.config/systemd/user
# note: not with `sudo`
$ systemctl --user daemon-reload
$ systemctl --user enable --now forgetful_folders.service
```
