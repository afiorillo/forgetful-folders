#!/usr/bin/env python

import logging
from hashlib import sha256
from pathlib import Path
from os import environ, pathsep, readlink
from sys import argv, exit, stderr
from typing import Union

# Linux only support
TEMP_DIR = Path('/dev/shm').resolve()

DEFAULT_DIRS = '~/.ramdisk'

ENVIRONMENT_DIRS = environ.get("FORGETFUL_FOLDERS", DEFAULT_DIRS).split(pathsep)

# Real logging 
LOG_LEVEL = environ.get("LOG_LEVEL", "INFO")
logging.basicConfig(level=LOG_LEVEL, format='[%(asctime)s] %(msg)s', stream=stderr)
log = logging.getLogger('forgetful_folders')

def load_pathname(pathname: Union[str, Path]) -> Path:
    return Path(pathname).expanduser()

def make_targetdir(original_dir: Path) -> Path:
    """
    Given an absolute path, return a unique path inside the tmpfs.
    It must be an existing directory.
    """
    dirhash = sha256(str(original_dir).encode()).hexdigest()[:8]
    tempdir = TEMP_DIR.joinpath(dirhash)
    tempdir.mkdir(parents=False, exist_ok=True)
    return tempdir

def make_forgetful_folder(dir: Path):
    # if the parent directory does not exist, then fail
    if not dir.parent.exists():
        raise ValueError(f"`{dir.parent}` parent directory does not exist")
    
    # make the path absolute, even if it doesn't yet exist
    dir = dir.parent.resolve().joinpath(dir.name)
    target = make_targetdir(dir)
    log.info(f'ensuring link `{dir}` -> `{target}`')

    # if the directory already exists and is not a link, then fail
    if dir.exists():
        if not dir.is_symlink():
            raise ValueError(f'`{dir}` already exists as a non-symlink')
        elif Path(readlink(dir)) != target:
        # elif dir.readlink() != target:  # would work, but 3.9+ only
            raise ValueError(f'`{dir}` already exists as a symbolic link pointing elsewhere')
        elif Path(readlink(dir)) == target:
        # elif dir.readlink() == target:
            return  # no link to make, we're done here

    # then make the link
    dir.symlink_to(target, target_is_directory=True)

def main():
    if len(argv) < 2:
        dirs = [load_pathname(fp) for fp in ENVIRONMENT_DIRS]
    elif len(argv) >= 2:
        dirs = [load_pathname(fp) for fp in argv[1:]]

    exitcode = 0
    for dir in dirs:
        try:
            make_forgetful_folder(dir)
        except:
            log.exception(f"unable to make forgetful folder {dir}")
            exitcode = 1
    exit(exitcode)

if __name__ == "__main__":
    main()
    stderr.flush()
